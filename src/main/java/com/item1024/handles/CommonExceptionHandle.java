package com.item1024.handles;

import com.item1024.beans.ApiResult;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestControllerAdvice
public class CommonExceptionHandle {
    protected Logger logger = Logger.getLogger(CommonExceptionHandle.class);

    @ExceptionHandler(value = CommonException.class)
    @ResponseBody()
    public ApiResult defaultHandle(CommonException e) throws IOException {

        if (e.getLogflag()) {
            logger.error("自定义错误处理", e);
        }

        if (e.getDebug() != null) {
            logger.debug(e.getDebug());
        }
        ApiResult apiResult = new ApiResult(e.getCode().ordinal(), e.getMessage());

        return apiResult;
    }
}
