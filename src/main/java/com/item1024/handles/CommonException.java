package com.item1024.handles;

import com.item1024.beans.ApiResult.ApiResultCode;

public class CommonException extends RuntimeException {
    private ApiResultCode code;
    private String message;
    private Boolean logflag = false;
    private Object debug;

    public CommonException(ApiResultCode code) {
        this.code = code;
    }

    public CommonException(ApiResultCode code, String message) {
        this.code = code;
        this.message = message;
    }

    public CommonException(ApiResultCode code, String message, Boolean logflag) {
        this.code = code;
        this.message = message;
        this.logflag = logflag;
    }

    public CommonException(ApiResultCode code, String message, Object debug) {
        this.code = code;
        this.message = message;
        this.debug = debug;
    }

    public CommonException(ApiResultCode code, String message, Boolean logflag, Object debug) {
        this.code = code;
        this.message = message;
        this.logflag = logflag;
        this.debug = debug;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ApiResultCode getCode() {
        return code;
    }

    public void setCode(ApiResultCode code) {
        this.code = code;
    }

    public Boolean getLogflag() {
        return logflag;
    }

    public void setLogflag(Boolean logflag) {
        this.logflag = logflag;
    }

    public Object getDebug() {
        return debug;
    }

    public void setDebug(Object debug) {
        this.debug = debug;
    }
}
