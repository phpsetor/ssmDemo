package com.item1024.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constants {

    @Value("${test}")
    private String test;

    @Value("${prop}")
    void initConstans(String prop) {
        PROP = prop;
        TEST = test;
    }

    public static String PROP;
    public static String TEST;


}
