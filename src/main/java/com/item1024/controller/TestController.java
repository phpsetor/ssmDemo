package com.item1024.controller;

import com.alibaba.fastjson.JSON;
import com.item1024.beans.ApiResult;
import com.item1024.config.Constants;
import com.item1024.handles.CommonException;
import com.item1024.mapper.UserMapper;
import com.item1024.pojo.User;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Api
@Controller
@RequestMapping("/test")
public class TestController {

    private Logger logger = Logger.getLogger(TestController.class);

    @Autowired
    private UserMapper userMapper;


    @PostMapping(value = "/index")
    public void test(@RequestParam(value = "id",required = false) int id,
                       HttpServletRequest request) throws CommonException {
        logger.info(id);

        logger.info(Constants.PROP);
        logger.info(Constants.TEST);
        User user = userMapper.selectByPrimaryKey(id);
        logger.info(JSON.toJSON(user));
        throw new CommonException(ApiResult.ApiResultCode.ERR_AUTH,"错误");
    }
}
