**这是一个个人代码记录仓库 方便直接使用 欢迎大家一起交流学习**

# beta 0.0.1:
 * spring5.0.6+springmvc5.0.6+mybatis1.3.2
 * 数据源HikariCP3.2.0
 * 增加swagger
 * 增加控制器切面 HttpAspect
 * 增加公共异常处理 CommonException
 * 增加多环境pom控制
 * 增加配置文件属性获取